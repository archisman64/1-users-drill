const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/ 

//      1       //
function findUsersInterestedInVideoGames (userList) {
    const usersInterestedInVideoGames = [];
    Object.entries(userList).map((element) => {
        const [userName, userDetails] = element;
        const interests = userDetails.interests;
        // console.log(interests)
        const result = interests.map((interest) => {
            return interest.toLowerCase().includes('video games');
        })
        
        if(result[0]) {
            usersInterestedInVideoGames.push(element);
        }
    })
    return usersInterestedInVideoGames;
}
console.log(findUsersInterestedInVideoGames(users));

//      2       //
function findUsersInGermany (userList) {
    const usersInGermany = Object.entries(userList).filter((element) => {
        const [userName, userDetails] = element;
        if(userDetails.nationality.trim().toLowerCase() === 'germany'){
            return true;
        } else {
            return false;
        }
    })
    return usersInGermany;
}
console.log('Users living in Germany:', findUsersInGermany(users));

//      3       //
function sortUsersBySeniority (userList) {
    const seniors = [];
    const juniors = [];
    const interns = [];
    Object.entries(userList).map((element) => {
        const [userName, userDetails] = element;
        const seniorityLevel = userDetails.desgination.trim().toLowerCase();

        if(seniorityLevel.includes('senior')){
            seniors.push(element);
        } else if (seniorityLevel.includes('developer')) {
            juniors.push(element);
        } else {
            interns.push(element);
        }
    });
    const sortedSeniors = seniors.sort((firstPerson, secondPerson) => {
        return secondPerson[1].age - firstPerson[1].age;
    });
    const sortedJuniors = juniors.sort((firstPerson, secondPerson) => {
        return secondPerson[1].age - firstPerson[1].age;
    });
    const sortedInterns = interns.sort((firstPerson, secondPerson) => {
        return secondPerson[1].age - firstPerson[1].age;
    });

    return [...sortedSeniors, ...sortedJuniors, ...sortedInterns];
}
console.log('sorted developers:', sortUsersBySeniority(users));

//      4       //
function findUsersWithMasters (userList) {
    const usersWithMasters = Object.entries(userList).filter((user) => {
        const [name, details] = user;
        if(details.qualification.trim().toLowerCase().includes('masters')){
            return true;
        } else {
            return false;
        }
    });
    return usersWithMasters;
}
console.log('Users with Masters:', findUsersWithMasters(users));

//      5       //
function groupByProgrammingLanguage (userList) {
    const group = { 
        'golang': [],
        'javascript': [],
        'python': []
    };
    Object.entries(userList).map((user) => {
        const [name, details] = user;
        const designation = details.desgination.trim().toLowerCase();

        if(designation.includes('golang')) {
            group['golang'].push(JSON.stringify(user));
        } else if (designation.includes('python')) {
            group['python'].push(JSON.stringify(user));
        } else {
            group['javascript'].push(JSON.stringify(user));
        }
    });
    return group;
}
console.log('Users grouped by programming language:', groupByProgrammingLanguage(users));